﻿using UnityEngine;

namespace Assets.UDP_Module.Scripts
{
    public class Filtering : MonoBehaviour {

        public static bool ApplyDelta = true, CanSaveFilterValues;
        private static float _delta;
        public static float Alpha = 0.90f;
        private static readonly float[] NInput = new float[3];
        private static readonly float[] NOutput = new float[3];
        public static Vector3 FilteredPosition;

        private void Update()
        {
            _delta = ApplyDelta ? Time.deltaTime : 1.0f;
        }

        public static Vector3 ApplyFiltering(Vector3 scale)
        {
            NInput[0] = scale.x;
            NInput[1] = scale.y;
            NInput[2] = scale.z;

            NOutput[0] = LowPass(NInput, NOutput)[0];
            NOutput[1] = LowPass(NInput, NOutput)[1];
            NOutput[2] = LowPass(NInput, NOutput)[2];
            
            FilteredPosition = new Vector3(LowPass(NInput, NOutput)[0], LowPass(NInput, NOutput)[1], LowPass(NInput, NOutput)[2]);

            return FilteredPosition;
        }


        public static float[] LowPass(float[] input, float[] output)
        {
            for (var i = 0; i < input.Length; i++)
            {
                if (output == null || output.Length == 0 || float.IsNaN(output[i]) || float.IsInfinity(output[i]) || float.IsNaN(output[i]))
                {
                    if (output != null) output[i] = input[i];
                }
                else
                    output[i] = output[i] + Alpha * (input[i] - output[i]) * _delta;
            }
            
            return output;
        }


        /// <summary>
        /// Making sure that any filtering values changed post-calibration are saved on the log file when application is closed
        /// </summary>
        private void OnApplicationQuit()
        {
            if(CanSaveFilterValues)
                CalibrationData.SaveCalibValues(UdpReceiver.Port, UdpData.Selection,
                                CalibrationProcess.Min.x, CalibrationProcess.Min.y, CalibrationProcess.Min.z,
                                CalibrationProcess.Max.x, CalibrationProcess.Max.y, CalibrationProcess.Max.z, Alpha, ApplyDelta);
        }
    }
}
