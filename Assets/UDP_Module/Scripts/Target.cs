﻿using UnityEngine;

namespace Assets.UDP_Module.Scripts
{
    public class Target : MonoBehaviour
    {
        public Vector3 MinLimits;
        public Vector3 MaxLimits;

        private void Update ()
        {
            KeepWithinLimits();
            
        }

        private void KeepWithinLimits()
        {
            transform.position = CalibrationProcess.NewPosition;

            if (transform.position.x < MinLimits.x)
                transform.position = new Vector3(MinLimits.x, transform.position.y, transform.position.z);

            if (transform.position.x > MaxLimits.x)
                transform.position = new Vector3(MaxLimits.x, transform.position.y, transform.position.z);

            if (transform.position.y < MinLimits.y)
                transform.position = new Vector3(transform.position.x, MinLimits.y, transform.position.z);

            if (transform.position.y > MaxLimits.y)
                transform.position = new Vector3(transform.position.x, MaxLimits.y, transform.position.z);

            if (transform.position.z < MinLimits.z)
                transform.position = new Vector3(transform.position.x, transform.position.y, MinLimits.z);

            if (transform.position.z > MaxLimits.z)
                transform.position = new Vector3(transform.position.x, transform.position.y, MaxLimits.z);
        }
    }
}
