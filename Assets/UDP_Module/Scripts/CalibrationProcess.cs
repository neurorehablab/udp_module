﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.UDP_Module.Scripts
{
    public class CalibrationProcess : MonoBehaviour
    {
        public static bool Calibrate;
        public static bool CalibrateX, CalibrateY, CalibrateZ;
        private static float _calibrationTime;
        private const float StepTime = 6.0f;
        public GameObject[] Arrows = new GameObject[6];
        private static readonly bool[] Steps = new bool[6];
        private static int _step;
        public static Vector3 NewPosition;
        public static bool StepSet;
        
        /// <summary>
        /// Minimum and maximum values of the different world coordinates 
        /// </summary>
        private Vector3 _rangeMax = new Vector3(10, 5.5f, 1);
        private Vector3 _rangeMin = new Vector3(-10, -5.5f, -1);

        /// <summary>
        /// Minimum and maximum UDP values to use in the scale calculation
        /// </summary>
        public static Vector3 Max = new Vector3(10, 5.5f, 1);
        public static Vector3 Min = new Vector3(-10, -5.5f, -1);

        public static Vector3 Scale;

        private void Update()
        {
            if (Calibrate && _step < 6)
                CalibrationSteps();
            else if (_step > 5)
                EndCalibrationProcess();

            CalculateScale();
            NewPosition = Filtering.ApplyFiltering(Scale);
        }

        private void CalculateScale()
        {
            var udppos = UdpData.UdpPos;
            var scalex = (udppos.x - Min.x) * ((_rangeMax.x - _rangeMin.x) / (Max.x - Min.x)) + _rangeMin.x;
            var scaley = (udppos.y - Min.y) * ((_rangeMax.y - _rangeMin.y) / (Max.y - Min.y)) + _rangeMin.y;
            var scalez = (udppos.z - Min.z) * ((_rangeMax.z - _rangeMin.z) / (Max.z - Min.z)) + _rangeMin.z;

            if (float.IsInfinity(scalex) || float.IsNaN(scalex))
                scalex = NewPosition.x;

            if (float.IsInfinity(scaley) || float.IsNaN(scaley))
                scaley = NewPosition.y;

            if (float.IsInfinity(scalez) || float.IsNaN(scalez))
                scalez = NewPosition.z;

            Scale = new Vector3(scalex, scaley, scalez);
            
        }

        /// <summary>
        /// This method is called when the device is chosen in the CalibrationUINavigation and determines which steps to execute according to chosen axis
        /// </summary>
        public static void SetCalibrationSteps()
        {
            _calibrationTime = StepTime;
            for (var i = 0; i < Steps.Length; i++)
                Steps[i] = false;
            
            if (CalibrateX)
            {
                _step = 0;
                StepSet = true;
                Steps[0] = true;
                Steps[1] = true;
            }

            if (CalibrateY)
            {
                if (!StepSet)
                {
                    _step = 2;
                    StepSet = true;
                }

                Steps[2] = true;
                Steps[3] = true;
            }

            if (CalibrateZ)
            {
                if (!StepSet)
                {
                    _step = 4;
                    StepSet = true;
                }
                Steps[4] = true;
                Steps[5] = true;
            }
        }
        
        /// <summary>
        /// Cowntdown time for each step, when reaching zero will determine next step
        /// </summary>
        private void CalibrationSteps()
        {
            _calibrationTime -= Time.deltaTime;

            CalibrateIndividualSteps();

            if (!Arrows[_step].activeSelf)
                Arrows[_step].SetActive(true);

            if (_calibrationTime < 0)
            {
                foreach (var t in Arrows)
                    t.SetActive(false);

                _calibrationTime = StepTime;

                Steps[_step] = false;
                if (_step < 5)
                {
                    if (Steps[_step + 1])
                        _step ++;
                    else if (_step < 4 && Steps[_step + 2])
                        _step = _step + 2;
                    else if (_step < 3 && Steps[_step + 3])
                        _step = _step + 3;
                    else
                        _step = 6;
                }
                else
                    EndCalibrationProcess();
            }     
        }

        /// <summary>
        /// Execute calibration depending of the actual step
        /// </summary>
        private void CalibrateIndividualSteps()
        {
            switch (_step)
            {
                case 0:
                    CalibrateLeft();
                    break;
                case 1:
                    CalibrateRight();
                    break;
                case 2:
                    CalibrateDown();
                    break;
                case 3:
                    CalibrateTop();
                    break;
                case 4:
                    CalibrateBack();
                    break;
                case 5:
                    CalibrateFront();
                    break;
            }
        }

        private void CalibrateLeft()
        {
            var xValues = new List<float> {UdpData.UdpPos.x};
            var mean = GetListAverage(xValues);
            Min = new Vector3(mean, Min.y, Min.z);
        }

        private void CalibrateRight()
        {
            var xValues = new List<float> {UdpData.UdpPos.x};
            var mean = GetListAverage(xValues);
            Max = new Vector3(mean, Max.y, Max.z);
        }

        private void CalibrateDown()
        {
            var yValues = new List<float> {UdpData.UdpPos.y};
            var mean = GetListAverage(yValues);
            Min = new Vector3(Min.x, mean, Min.z);
        }

        private void CalibrateTop()
        {
            var yValues = new List<float> {UdpData.UdpPos.y};
            var mean = GetListAverage(yValues);
            Max = new Vector3(Max.x, mean, Max.z);
        }

        private void CalibrateBack()
        {
            var zValues = new List<float> {UdpData.UdpPos.z};
            var mean = GetListAverage(zValues);
            Min = new Vector3(Min.x, Min.y, mean);
        }

        private void CalibrateFront()
        {
            var zValues = new List<float> {UdpData.UdpPos.z};
            var mean = GetListAverage(zValues);
            Max = new Vector3(Max.x, Max.y, mean);
        }

        private void EndCalibrationProcess()
        {
            Calibrate = false;
            _step = 0;
            foreach (var t in Arrows)
            {
                t.SetActive(false);
            }
            Debug.Log("Calibration finished");
            CalibrationData.SaveCalibValues(UdpReceiver.Port, UdpData.Selection, Min.x, Min.y, Min.z, Max.x, Max.y, Max.z, Filtering.Alpha, Filtering.ApplyDelta);
            Filtering.CanSaveFilterValues = true;
        }

        private float GetListAverage(List<float> list)
        {
            var sum = list.Sum();
            var average = sum / list.Count;

            return average;
        }
    }
}
