﻿using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.UDP_Module.Scripts
{
    public class CalibrationUiNavigation : MonoBehaviour
    {
        public Button Network, FilteringBtn, Devices, Connect;
        public GameObject NetworkPanel, FilteringPanel, DevicesPanel;
        public InputField PortInputfield;
        public Text IpText, Min, Max, Scale, Filtered, FilterValue;
        public Toggle Calibrate, UseX, UseY, UseZ, Delta;
        public Slider Filter;
        public GameObject DevicesScroll;

        private void Start()
        {
            Network.Select();
            NetworkPanel.SetActive(true);
            FilteringPanel.SetActive(false);
            DevicesPanel.SetActive(false);
            IpText.text = "Local IP: " + LocalIpAddress();
            Calibrate.isOn = true;
            
        }

        private void Update()
        {
            if (FilteringPanel.activeSelf)
            {
                Min.text = "Min: " + CalibrationProcess.Min;
                Max.text = "Max: " + CalibrationProcess.Max;
                Scale.text = "Scale: " + CalibrationProcess.Scale;
                Filtered.text = "Filtered: " + Filtering.FilteredPosition;
                FilterValue.text = Filtering.Alpha.ToString("0.00");
                Filter.value = Filtering.Alpha;
                Delta.isOn = Filtering.ApplyDelta;
            }
            Connect.gameObject.GetComponentInChildren<Text>().text = UdpReceiver.Connected ? "DISCONNECT" : "CONNECT";
        }

        public void UpdateToggles()
        {
            CalibrationProcess.CalibrateX = UseX.isOn;
            CalibrationProcess.CalibrateY = UseY.isOn;
            CalibrationProcess.CalibrateZ = UseZ.isOn;
            Filtering.ApplyDelta = Delta.isOn;
        }
        
        public void UdpConnect()
        {
            if (!UdpReceiver.Connected)
            {
                UdpReceiver.Connected = true;
                UdpReceiver.Port = int.Parse(PortInputfield.text);
            }
            else
            {
                UdpReceiver.Connected = false;   
            }
        }

        public void AdjustAlphaFilter(float filter)
        {
            Filtering.Alpha = filter;
            FilterValue.text = filter.ToString("0.00");
        }

        /// <summary>
        /// gets local ip address to display in the UI
        /// </summary>
        /// <returns></returns>
        private string LocalIpAddress()
        {
            var localIp = "";
            var host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                    localIp = ip.ToString();
            }

            return localIp;
        }


        /// <summary>
        /// When Devices button is pressed, all the available devices are displayed in the DevicesScroll UI object
        /// if one of the devices is chosen the tracking + calibration starts (if enabled)
        /// </summary>
        public void ListDevices()
        {
            var devices = GameObject.FindGameObjectsWithTag("Device");
            foreach (var t in devices)
                Destroy(t);

            for (var i = 0; i < UdpData.Devices.Count; i++)
            {
                var newDevice = Instantiate(Resources.Load("Device")) as GameObject;

                if (newDevice != null)
                {
                    newDevice.name = UdpData.Devices[i];
                    newDevice.transform.SetParent(DevicesScroll.transform, false);

                    var btn = newDevice.GetComponent<Button>();
                    var btnText = btn.GetComponentInChildren<Text>();
                    btnText.text =
                        System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(
                            UdpData.Devices[i].ToUpper());

                    var temp = i;

                    btn.onClick.AddListener(() =>
                    {
                        UpdateToggles();
                        UdpData.Selection = UdpData.Devices[temp];
                        CalibrationProcess.StepSet = false;
                        CalibrationProcess.SetCalibrationSteps();
                        CalibrationProcess.Calibrate = Calibrate.isOn;
                        if (!CalibrationProcess.Calibrate)
                            CalibrationData.SaveCalibValues(UdpReceiver.Port, UdpData.Selection,
                                CalibrationProcess.Min.x, CalibrationProcess.Min.y, CalibrationProcess.Min.z,
                                CalibrationProcess.Max.x, CalibrationProcess.Max.y, CalibrationProcess.Max.z, Filtering.Alpha, Filtering.ApplyDelta);
                        gameObject.SetActive(false);
                    });

                    if (i > 0)
                        DevicesScroll.GetComponent<RectTransform>().sizeDelta =
                            new Vector2(DevicesScroll.GetComponent<RectTransform>().sizeDelta.x, 100.0f*i);
                }
            }           
        }
    }
}
