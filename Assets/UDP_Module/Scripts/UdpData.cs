﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.UDP_Module.Scripts
{
    public class UdpData : MonoBehaviour {

        public static List<string> Devices = new List<string>();
        public static string Selection = "n/a";
        public static Vector3 UdpPos;
        public static bool CanListDevices;

        /// <summary>
        /// Decompose incoming data based on the protocol rules: [$]dataType , [$$]device , [$$$]joint , transformation , param_1 , param_2 , .... , param_N
        /// </summary>
        /// <param name="nData">_message from UdpReceiver</param>
        public static void TranslateData(string nData)
        {
            string[] separators = { "[$]", "[$$]", "[$$$]", ",", ";", " " };
            var words = nData.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            
            var device = words[1];
            var joint = words[2];
            var transformationType = words[3];

            var emustring = device + "_" + joint;

            //populate list of devices
            if (!Devices.Contains(emustring) && emustring != string.Empty)
                Devices.Add(emustring);//emulation

            //updates the UdpPos Vector3 coordinates based on raw incoming data after selecting a device
            if (Selection == emustring && transformationType == "position")
            {
                var z = 0.0f;
                if (words.Length > 6)
                    z = float.Parse(words[6]);

                UdpPos = new Vector3(float.Parse(words[4]), float.Parse(words[5]), z);
                //Debug.Log("UdpPos: " + UdpPos);
            }
        }

        public static void ClearList()
        {
            Devices.Clear();
        }   
    }
}
