﻿using System;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

namespace Assets.UDP_Module.Scripts
{
    public class UdpReceiver : MonoBehaviour
    {
        public GameObject CalibrationUi;
        private Thread _receiveThread;
        private UdpClient _client;
        private bool _isConnected;

        public static int Port = 1202;
        public static bool Connected;

        private string _message = "";

        public void Start()
        {
            var calibValues = CalibrationData.LoadCalibValues();
            
            if (calibValues != null)
            {
                Port = int.Parse(calibValues[0]);
                UdpData.Selection = calibValues[1];
                CalibrationProcess.Min = new Vector3(float.Parse(calibValues[2]), float.Parse(calibValues[3]),float.Parse(calibValues[4]));
                CalibrationProcess.Max = new Vector3(float.Parse(calibValues[5]), float.Parse(calibValues[6]), float.Parse(calibValues[7]));
                Filtering.Alpha = float.Parse(calibValues[8]);
                Filtering.ApplyDelta = bool.Parse(calibValues[9]);
                Connected = true;
                CalibrationUi.SetActive(false);
                Filtering.CanSaveFilterValues = true;
            }
            DontDestroyOnLoad(gameObject);
        }

        private void Update()
        {
            if (Connected && !_isConnected)
            {
                Init();
            }
            else if (!Connected && _isConnected)
            {
                _receiveThread.Abort();
                _client.Close();
                UdpData.ClearList();
            }

            if (Input.GetKeyDown(KeyCode.U))
                CalibrationUi.SetActive(!CalibrationUi.activeSelf);
        }

        /// <summary>
        /// create new thread to receive incoming messages.
        /// </summary>
        private void Init()
        {
            _receiveThread = new Thread(ReceiveData) {IsBackground = true};
            _receiveThread.Start();
            _isConnected = true;
        }

        /// <summary>
        /// receives thread from local IP, incodes received message in text format and invokes the TranslateData method from UdpData
        /// </summary>
        private void ReceiveData()
        {
            _client = new UdpClient(Port);

            while (_isConnected)
            {
                try
                {
                    var anyIp = new IPEndPoint(IPAddress.Loopback, 0);//IPAddress.Loopback equivalent to 127.0.0.1 

                    var data = _client.Receive(ref anyIp);

                    _message = Encoding.UTF8.GetString(data);

                    //_message goes first through an initial split process to detect if it refers to the kinect full package or not
                    if (_message != string.Empty)
                    {
                        string[] sep = { ";" };
                        var wordsplit = _message.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                        foreach (var wsp in wordsplit)
                        {
                            UdpData.TranslateData(wsp.Length >= 5 ? wsp : _message);
                        }
                    }
                }
                catch (Exception err)
                {
                    print(err.ToString());
                }
            }
        }
        
        private void OnDisable()
        {
            if (_receiveThread != null)
            {
                _isConnected = false;
                _receiveThread.Abort();
                _client.Close();
            }
        }

        private void OnApplicationQuit()
        {
            if (_receiveThread != null)
            {
                _isConnected = false;
                _receiveThread.Abort();
                _client.Close();
            }
        }
    }
}


