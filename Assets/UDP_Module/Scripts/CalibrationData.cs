﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using UnityEngine;

namespace Assets.UDP_Module.Scripts
{
    public class CalibrationData : MonoBehaviour {

        private static string _filepath;
        private static XmlDocument _xmlDoc;
        private static XmlWriter _writer;
        private static string _path;

        public static void SaveCalibValues(int port, string deviceAndJoint, float xmin, float ymin, float zmin, float xmax, float ymax, float zmax, float alpha, bool delta)
        {
            _path = Application.dataPath + "/CalibrationData/";
            _filepath = _path + "calibration.xml";
            if (!Directory.Exists(_path))
            {
                Directory.CreateDirectory(_path);
            }

            var settings = new XmlWriterSettings
            {
                Indent = true,
                NewLineOnAttributes = true
            };
            _writer = XmlWriter.Create(_filepath, settings);
            _writer.WriteStartDocument();

            _writer.WriteStartElement("Config");

            _writer.WriteElementString("Port", port.ToString());
            _writer.WriteElementString("DeviceAndJoint", deviceAndJoint);
            _writer.WriteElementString("CalibXmin", xmin.ToString(CultureInfo.InvariantCulture));
            _writer.WriteElementString("CalibYmin", ymin.ToString(CultureInfo.InvariantCulture));
            _writer.WriteElementString("CalibZmin", zmin.ToString(CultureInfo.InvariantCulture));
            _writer.WriteElementString("CalibXmax", xmax.ToString(CultureInfo.InvariantCulture));
            _writer.WriteElementString("CalibYmax", ymax.ToString(CultureInfo.InvariantCulture));
            _writer.WriteElementString("CalibZmax", zmax.ToString(CultureInfo.InvariantCulture));
            _writer.WriteElementString("Alpha", alpha.ToString(CultureInfo.InvariantCulture));
            _writer.WriteElementString("Delta", delta.ToString());

            _writer.WriteEndElement();
            _writer.WriteEndDocument();
            _writer.Flush();
            _writer.Close();
        }

        public static List<string> LoadCalibValues()
        {
            _path = Application.dataPath + "/CalibrationData/";
            _filepath = _path + "calibration.xml";

            _xmlDoc = new XmlDocument();

            var calibValues = new List<string>();
            
            if (File.Exists(_filepath))
            {
                _xmlDoc.Load(_filepath);

                var configList = _xmlDoc.GetElementsByTagName("Config");

                foreach (XmlNode configInfo in configList)
                {
                    var xmlcontent = configInfo.ChildNodes;

                    foreach (XmlNode xmlsettings in xmlcontent)
                    {
                        calibValues.Add(xmlsettings.InnerText);
                    }
                }
            }
            else
            {
                return null;
            }

            return calibValues;
        }
    }
}
